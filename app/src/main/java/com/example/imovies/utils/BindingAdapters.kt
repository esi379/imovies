package com.example.imovies.utils

import android.annotation.SuppressLint
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestOptions
import com.example.imovies.data.api.URLs
import com.example.imovies.data.model.Genre
import java.text.DecimalFormat
import java.text.DecimalFormatSymbols

/** Load image from a link in image view */
@BindingAdapter("imageLink")
fun AppCompatImageView.loadImage(link: String?) {
    Glide.with(context)
        .load(URLs.IMAGE_URL + link)
        .transition(DrawableTransitionOptions.withCrossFade())
        .apply(
            RequestOptions()
                .placeholder(android.R.drawable.ic_menu_gallery)
                .error(android.R.drawable.ic_menu_report_image)
        )
        .into(this)
}

/** Show currency value in text view */
@SuppressLint("SetTextI18n")
@BindingAdapter("showCurrency")
fun AppCompatTextView.showCurrency(value: Int) {
    val symbols = DecimalFormatSymbols()
    symbols.decimalSeparator = ','
    val decimalFormat = DecimalFormat("###,###,###,###", symbols)
    text = decimalFormat.format(value) + " USD"
}

/** Show Genre in text view */
@BindingAdapter("showGenre")
fun AppCompatTextView.showGenre(value: ArrayList<Genre>) {
    var genres = ""
    value.forEach {
        genres += "${it.name}, "
    }
    text = genres.removeSuffix(", ")
}