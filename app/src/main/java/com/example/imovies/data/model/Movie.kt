package com.example.imovies.data.model

import com.google.gson.annotations.SerializedName

data class Movie(
    @SerializedName("id")
    var id: Long = 0,
    @SerializedName("adult")
    var adult: Boolean? = false,
    @SerializedName("backdrop_path")
    var preview: String? = "",
    @SerializedName("budget")
    var budget: Int? = 0,
    @SerializedName("genres")
    var genres: ArrayList<Genre> = arrayListOf(),
    @SerializedName("homepage")
    var homepage: String? = "",
    @SerializedName("production_companies")
    var pruducers: ArrayList<Company> = arrayListOf(),
    @SerializedName("original_language")
    var language: String? = "",
    @SerializedName("original_title", alternate = ["title"])
    var title: String? = "",
    @SerializedName("overview")
    var overview: String? = "",
    @SerializedName("release_date")
    var releaseDate: String? = "",
    @SerializedName("vote_average")
    var voteRate: Double? = 0.0,
    @SerializedName("vote_count")
    var voteCount: Int? = 0,
    @SerializedName("status")
    var status: String? = "",
    @SerializedName("poster_path")
    var poster: String? = "",
    @SerializedName("revenue")
    var revenue: Int? = 0,
    @SerializedName("runtime")
    var duration: Int? = 0,
)