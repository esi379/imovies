package com.example.imovies.data.repository

import com.example.imovies.app.Constants
import com.example.imovies.data.api.MainApiService
import com.example.imovies.data.model.networkBoundResource
import com.example.imovies.ui.main.MovieType
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.flowOn
import javax.inject.Inject


class MainRepository @Inject constructor(private val mainApiService: MainApiService) {

    /** Get movie list */
    fun getMovies(page: Int, type: MovieType) = networkBoundResource(
        fetch = {
            when (type) {
                MovieType.NOW_PLAYING -> mainApiService.fetchNowPlaying(
                    Constants.TOKEN_TYPE + " " + Constants.API_KEY,
                    page
                )
                MovieType.POPULAR -> mainApiService.fetchPopular(
                    Constants.TOKEN_TYPE + " " + Constants.API_KEY,
                    page
                )
                MovieType.TOP_RATED -> mainApiService.fetchTopRated(
                    Constants.TOKEN_TYPE + " " + Constants.API_KEY,
                    page
                )
                else -> mainApiService.fetchUpcoming(
                    Constants.TOKEN_TYPE + " " + Constants.API_KEY,
                    page
                )
            }
        }
    ).flowOn(Dispatchers.IO)

    /** Get movie details */
    fun getMovieDetails(movieId: Int) = networkBoundResource(
        fetch = {
            mainApiService.fetchMovieDetails(
                Constants.TOKEN_TYPE + " " + Constants.API_KEY,
                movieId
            )
        }
    ).flowOn(Dispatchers.IO)

    /** Get movie trailers */
    fun getMovieTrailers(movieId: Int) = networkBoundResource(
        fetch = {
            mainApiService.getMovieTrailers(Constants.TOKEN_TYPE + " " + Constants.API_KEY, movieId)
        }
    ).flowOn(Dispatchers.IO)

    /** Get movie cast */
    fun getMovieCast(movieId: Int) = networkBoundResource(
        fetch = {
            mainApiService.getMovieCast(Constants.TOKEN_TYPE + " " + Constants.API_KEY, movieId)
        }
    ).flowOn(Dispatchers.IO)

    /** Get keywords */
    fun getKeywords(query: String, page: Int) = networkBoundResource(
        fetch = {
            mainApiService.getKeywords(Constants.TOKEN_TYPE + " " + Constants.API_KEY, query, page)
        }
    ).flowOn(Dispatchers.IO)

    /** Get keywords */
    fun getSearchResult(query: String, page: Int) = networkBoundResource(
        fetch = {
            mainApiService.getSearch(Constants.TOKEN_TYPE + " " + Constants.API_KEY, query, page)
        }
    ).flowOn(Dispatchers.IO)
}