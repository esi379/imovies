package com.example.imovies.data.model

import com.google.gson.annotations.SerializedName


class PagingList<ResultType> {
    @SerializedName("results", alternate = ["cast"])
    var list: MutableList<ResultType> = arrayListOf()

    @SerializedName("total_pages")
    var lastPage: Int = Int.MAX_VALUE
}