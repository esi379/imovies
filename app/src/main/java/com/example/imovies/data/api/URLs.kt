package com.example.imovies.data.api

object URLs {

    const val BASE_URL = "https://api.themoviedb.org"

    const val MOVIES_URL = "/3/movie/"

    const val SEARCH_URL = "/3/search/"

    const val IMAGE_URL = "https://image.tmdb.org/t/p/original"

    const val YOUTUBE_URL = "https://www.youtube.com/watch?v="
}