package com.example.imovies.data.api

import com.example.imovies.app.Constants
import com.example.imovies.data.model.*
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Path
import retrofit2.http.Query

interface MainApiService {

    @GET(URLs.MOVIES_URL + "now_playing")
    suspend fun fetchNowPlaying(
        @Header(Constants.AUTHENTICATION) token: String,
        @Query("page") page: Int
    ): Response<PagingList<Movie>>

    @GET(URLs.MOVIES_URL + "popular")
    suspend fun fetchPopular(
        @Header(Constants.AUTHENTICATION) token: String,
        @Query("page") page: Int
    ): Response<PagingList<Movie>>

    @GET(URLs.MOVIES_URL + "top_rated")
    suspend fun fetchTopRated(
        @Header(Constants.AUTHENTICATION) token: String,
        @Query("page") page: Int
    ): Response<PagingList<Movie>>

    @GET(URLs.MOVIES_URL + "upcoming")
    suspend fun fetchUpcoming(
        @Header(Constants.AUTHENTICATION) token: String,
        @Query("page") page: Int
    ): Response<PagingList<Movie>>

    @GET(URLs.MOVIES_URL + "{movie_id}")
    suspend fun fetchMovieDetails(
        @Header(Constants.AUTHENTICATION) token: String,
        @Path("movie_id") movie_id: Int
    ): Response<Movie>

    @GET(URLs.MOVIES_URL + "{movie_id}/videos")
    suspend fun getMovieTrailers(
        @Header(Constants.AUTHENTICATION) token: String,
        @Path("movie_id") movie_id: Int
    ): Response<PagingList<Trailer>>

    @GET(URLs.MOVIES_URL + "{movie_id}/credits")
    suspend fun getMovieCast(
        @Header(Constants.AUTHENTICATION) token: String,
        @Path("movie_id") movie_id: Int
    ): Response<PagingList<Cast>>

    @GET(URLs.SEARCH_URL + "keyword")
    suspend fun getKeywords(
        @Header(Constants.AUTHENTICATION) token: String,
        @Query("query") query: String,
        @Query("page") page: Int
    ): Response<PagingList<Keyword>>

    @GET(URLs.SEARCH_URL + "movie")
    suspend fun getSearch(
        @Header(Constants.AUTHENTICATION) token: String,
        @Query("query") query: String,
        @Query("page") page: Int
    ): Response<PagingList<Movie>>
}