package com.example.imovies.data.model

import com.google.gson.annotations.SerializedName

data class Company(
    @SerializedName("logo_path")
    var logo: String? = "",
    @SerializedName("name")
    var name: String? = "",
)