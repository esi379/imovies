package com.example.imovies.data.model

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}