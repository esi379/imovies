package com.example.imovies.data.model

import com.google.gson.annotations.SerializedName

data class Trailer(
    @SerializedName("name")
    var name:String,
    @SerializedName("key")
    var key:String,
    @SerializedName("site")
    var site:String,
)
