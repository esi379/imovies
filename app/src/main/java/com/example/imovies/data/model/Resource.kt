package com.example.imovies.data.model

open class Resource<T> private constructor(
    private val status: Status,
    var data: T?,
    val message: String?
) {

    val isSuccess: Boolean
        get() = status === Status.SUCCESS && data != null

    val isError: Boolean
        get() = status === Status.ERROR

    val isLoading: Boolean
        get() = status === Status.LOADING

    companion object {

        fun <T> success(data: T): Resource<T> {
            return Resource(Status.SUCCESS, data, null)
        }

        fun <T> error(msg: String, data: T?): Resource<T> {
            return Resource(Status.ERROR, data, msg)
        }

        fun <T> loading(data: T?): Resource<T> {
            return Resource(Status.LOADING, data, null)
        }
    }
}