package com.example.imovies.data.model

import com.google.gson.annotations.SerializedName

data class Cast(
    @SerializedName("name")
    var name: String,
    @SerializedName("profile_path")
    var avatar: String?,
    @SerializedName("character")
    var character: String,
)