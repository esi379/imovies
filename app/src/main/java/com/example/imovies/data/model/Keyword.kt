package com.example.imovies.data.model

import com.google.gson.annotations.SerializedName

data class Keyword(
    @SerializedName("id")
    var id: Int,
    @SerializedName("name")
    var name: String
)
