package com.example.imovies.data.model


/**
 * Created by vv0z on 2021/06/19.
 *
 * @Description :
 */
class Result {

    companion object {
        const val STATUS_OK = 1
        const val HTTP_OK = 200
        const val INTERNAL_ERROR = 500
    }
}