package com.example.imovies.ui.search.datasource

import android.os.Build
import androidx.annotation.RequiresApi
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.asLiveData
import androidx.paging.ItemKeyedDataSource
import com.example.imovies.data.model.Keyword
import com.example.imovies.data.model.Movie
import com.example.imovies.data.model.PagingList
import com.example.imovies.data.model.Resource
import com.example.imovies.data.repository.MainRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

/**
 * Created by vv0z on 08.01.2022
 */
@RequiresApi(Build.VERSION_CODES.Q)
class ListDataSource<T>(
    private val type: String,
    private val query: String,
    private val repository: MainRepository,
    private val scope: CoroutineScope,
    private val networkHandler: MutableLiveData<Resource<PagingList<T>>>,
) : ItemKeyedDataSource<Int, T>() {

    private var page = 1
    private var lastPage = 1

    override fun getKey(item: T): Int {
        return if (item is Movie) {
            (item as Movie).id.toInt()
        } else {
            (item as Keyword).id
        }
    }

    override fun loadInitial(params: LoadInitialParams<Int>, callback: LoadInitialCallback<T>) {
        val request = if (type == "keywords") {
            repository.getKeywords(query, page).asLiveData(scope.coroutineContext)
        } else {
            repository.getSearchResult(query, page).asLiveData(scope.coroutineContext)
        }
        scope.launch(Dispatchers.Main) {
            request.observeForever {
                when {
                    it.isLoading -> networkHandler.postValue(Resource.loading(null))
                    it.isError -> networkHandler.postValue(
                        Resource.error(it.message!!, null)
                    )
                    it.isSuccess -> {
                        networkHandler.postValue(Resource.success(it.data!! as PagingList<T>))
                        lastPage = it.data!!.lastPage
                        it.data?.list?.let { it1 -> callback.onResult(it1 as MutableList<T>) }
                    }
                }
            }
        }
    }

    override fun loadBefore(params: LoadParams<Int>, callback: LoadCallback<T>) {
        //
    }

    override fun loadAfter(params: LoadParams<Int>, callback: LoadCallback<T>) {
        if (page < lastPage) {
            val request = if (type == "keywords") {
                repository.getKeywords(query, ++page).asLiveData(scope.coroutineContext)
            } else {
                repository.getSearchResult(query, ++page).asLiveData(scope.coroutineContext)
            }
            scope.launch(Dispatchers.Main) {
                request.observeForever {
                    when {
                        it.isLoading -> networkHandler.postValue(Resource.loading(null))
                        it.isError -> networkHandler.postValue(
                            Resource.error(it.message!!, null)
                        )
                        it.isSuccess -> {
                            networkHandler.postValue(Resource.success(it.data!! as PagingList<T>))
                            it.data?.list?.let { it1 -> callback.onResult(it1 as MutableList<T>) }
                        }
                    }
                }
            }
        }
    }
}