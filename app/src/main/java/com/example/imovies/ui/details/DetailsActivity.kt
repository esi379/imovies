package com.example.imovies.ui.details

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.activity.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.imovies.R
import com.example.imovies.data.api.URLs
import com.example.imovies.data.model.Trailer
import com.example.imovies.databinding.ActivityDetailsBinding
import com.example.imovies.ui.base.BaseActivity
import com.example.imovies.utils.showGenre
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_details.*
import kotlinx.android.synthetic.main.activity_main.loading

@AndroidEntryPoint
class DetailsActivity : BaseActivity<ActivityDetailsBinding>(),
    TrailersAdapter.OnItemClickListener {

    private val detailsViewModel: DetailsViewModel by viewModels()

    private var movieId: Int = 0

    private val trailerAdapter: TrailersAdapter by lazy {
        TrailersAdapter()
    }

    private val castAdapter: CastAdapter by lazy {
        CastAdapter()
    }

    override fun getLayoutId(): Int {
        return R.layout.activity_details
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        initUI()
        getMovieDetails()
        getTrailers()
        getCast()
    }

    /** Initialize UI */
    private fun initUI() {
        movieId = intent.extras?.getLong("movieId")?.toInt() ?: 0

        trailerAdapter.setListener(this)
        rv_related.apply {
            layoutManager = LinearLayoutManager(
                this@DetailsActivity,
                LinearLayoutManager.HORIZONTAL,
                false
            )
            adapter = trailerAdapter
        }

        rv_cast.apply {
            layoutManager = LinearLayoutManager(
                this@DetailsActivity,
                LinearLayoutManager.HORIZONTAL,
                false
            )
            adapter = castAdapter
        }
    }

    private fun getMovieDetails() {
        detailsViewModel.movieDetailsLiveData.invoke(movieId).observe(this, {
            when {
                it.isLoading -> loading.visibility = View.VISIBLE
                it.isSuccess -> {
                    loading.visibility = View.GONE
                    viewDataBinding?.movie = it.data
                    it.data?.let { it1 -> txt_genre.showGenre(it1.genres) }
                }
                it.isError -> {
                    loading.visibility = View.GONE
                    it.message?.let { it1 -> errorHandler(it1) }
                }
            }
        })

    }

    private fun getTrailers() {
        detailsViewModel.trailersLiveData.invoke(movieId).observe(this, {
            if (it.isSuccess) {
                it.data?.let { it1 -> trailerAdapter.setItems(it1.list) }
            }
        })
    }

    private fun getCast() {
        detailsViewModel.castLiveData.invoke(movieId).observe(this, {
            if (it.isSuccess) {
                it.data?.let { it1 -> castAdapter.setItems(it1.list) }
            }
        })
    }

    override fun onTrailerClick(item: Trailer) {
        startActivity(
            Intent(Intent.ACTION_VIEW, Uri.parse(URLs.YOUTUBE_URL + item.key))
        )
    }
}