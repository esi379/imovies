package com.example.imovies.ui.main.datasource

import android.os.Build
import androidx.annotation.RequiresApi
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.asLiveData
import androidx.paging.ItemKeyedDataSource
import com.example.imovies.data.model.Movie
import com.example.imovies.data.model.PagingList
import com.example.imovies.data.model.Resource
import com.example.imovies.data.repository.MainRepository
import com.example.imovies.ui.main.MovieType
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

/**
 * Created by vv0z on 06.01.2022
 */
@RequiresApi(Build.VERSION_CODES.Q)
class MovieListDataSource(
    private val type: MovieType,
    private val repository: MainRepository,
    private val scope: CoroutineScope,
    private val networkHandler: MutableLiveData<Resource<PagingList<Movie>>>,
) : ItemKeyedDataSource<Int, Movie>() {

    private var page = 1
    private var lastPage = 1

    override fun getKey(item: Movie): Int {
        return item.id.toInt()
    }

    override fun loadInitial(params: LoadInitialParams<Int>, callback: LoadInitialCallback<Movie>) {
        val request = repository.getMovies(page, type).asLiveData(scope.coroutineContext)
        scope.launch(Dispatchers.Main) {
            request.observeForever {
                when {
                    it.isLoading -> networkHandler.postValue(Resource.loading(null))
                    it.isError -> networkHandler.postValue(
                        Resource.error(it.message!!, null)
                    )
                    it.isSuccess -> {
                        networkHandler.postValue(Resource.success(it.data!!))
                        lastPage = it.data!!.lastPage
                        it.data?.list?.let { it1 -> callback.onResult(it1) }
                    }
                }
            }
        }
    }

    override fun loadBefore(params: LoadParams<Int>, callback: LoadCallback<Movie>) {
        //
    }

    override fun loadAfter(params: LoadParams<Int>, callback: LoadCallback<Movie>) {
        if (page < lastPage) {
            val request =
                repository.getMovies(++page, type).asLiveData(scope.coroutineContext)
            scope.launch(Dispatchers.Main) {
                request.observeForever {
                    when {
                        it.isLoading -> networkHandler.postValue(Resource.loading(null))
                        it.isError -> networkHandler.postValue(
                            Resource.error(it.message!!, null)
                        )
                        it.isSuccess -> {
                            networkHandler.postValue(Resource.success(it.data!!))
                            it.data?.list?.let { it1 -> callback.onResult(it1) }
                        }
                    }
                }
            }
        }
    }
}