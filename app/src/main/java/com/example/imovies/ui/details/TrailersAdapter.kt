package com.example.imovies.ui.details

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.imovies.data.model.Trailer
import com.example.imovies.databinding.TrailerItemViewBinding
import java.util.*

class TrailersAdapter:RecyclerView.Adapter<TrailersAdapter.ItemViewHolder>() {

    private var itemList: MutableList<Trailer> = mutableListOf()
    private var mOnItemClickListener: OnItemClickListener? = null

    interface OnItemClickListener {
        fun onTrailerClick(item: Trailer)
    }

    fun setListener(onItemClickListener: OnItemClickListener?) {
        this.mOnItemClickListener = onItemClickListener
    }

    inner class ItemViewHolder(val binding: TrailerItemViewBinding) :
        RecyclerView.ViewHolder(binding.root)

    fun setItems(newList: MutableList<Trailer>) {
        val diffCallback = ItemDiffCallback(this.itemList, newList)
        val diffResult = DiffUtil.calculateDiff(diffCallback)
        this.itemList = LinkedList(newList)
        diffResult.dispatchUpdatesTo(this)
    }

    private fun getItem(position: Int): Trailer {
        return itemList[position]
    }

    inner class ItemDiffCallback(
        private val mOldList: List<Trailer>,
        private val mNewList: List<Trailer>,
    ) : DiffUtil.Callback() {

        override fun getOldListSize(): Int {
            return mOldList.size
        }

        override fun getNewListSize(): Int {
            return mNewList.size
        }

        override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            return mOldList[oldItemPosition].key == mNewList[newItemPosition].key
        }

        override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            val oldItem = mOldList[oldItemPosition]
            val newItem = mNewList[newItemPosition]
            return (oldItem == newItem)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = TrailerItemViewBinding.inflate(layoutInflater, parent, false)
        return ItemViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        val item = getItem(position)

        holder.binding.trailer = item

        holder.itemView.setOnClickListener {
            mOnItemClickListener?.onTrailerClick(item)
        }
    }

    override fun getItemCount(): Int {
        return itemList.size
    }
}