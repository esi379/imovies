package com.example.imovies.ui.main

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.activity.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.imovies.R
import com.example.imovies.data.model.Movie
import com.example.imovies.databinding.ActivityMainBinding
import com.example.imovies.ui.base.BaseActivity
import com.example.imovies.ui.details.DetailsActivity
import com.example.imovies.ui.search.SearchActivity
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.view_toolbar.*

@AndroidEntryPoint
class MainActivity : BaseActivity<ActivityMainBinding>(), MoviesAdapter.OnItemClickListener {

    private val mainViewModel: MainViewModel by viewModels()

    private val nowPlayingAdapter: MoviesAdapter by lazy {
        MoviesAdapter()
    }

    private val popularAdapter: MoviesAdapter by lazy {
        MoviesAdapter()
    }

    private val topRatedAdapter: MoviesAdapter by lazy {
        MoviesAdapter()
    }

    private val upcomingAdapter: MoviesAdapter by lazy {
        MoviesAdapter()
    }

    override fun getLayoutId(): Int {
        return R.layout.activity_main
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        initUI()
        getNowPlaying()
        getPopular()
        getTopRated()
        getUpcoming()
    }

    /** Initialize UI */
    private fun initUI() {
        nowPlayingAdapter.setOnItemClickListener(this)
        rv_now_playing.apply {
            layoutManager = LinearLayoutManager(
                this@MainActivity,
                LinearLayoutManager.HORIZONTAL,
                false
            )
            adapter = nowPlayingAdapter
        }

        popularAdapter.setOnItemClickListener(this)
        rv_popular.apply {
            layoutManager = LinearLayoutManager(
                this@MainActivity,
                LinearLayoutManager.HORIZONTAL,
                false
            )
            adapter = popularAdapter
        }

        topRatedAdapter.setOnItemClickListener(this)
        rv_toprated.apply {
            layoutManager = LinearLayoutManager(
                this@MainActivity,
                LinearLayoutManager.HORIZONTAL,
                false
            )
            adapter = topRatedAdapter
        }

        upcomingAdapter.setOnItemClickListener(this)
        rv_upcoming.apply {
            layoutManager = LinearLayoutManager(
                this@MainActivity,
                LinearLayoutManager.HORIZONTAL,
                false
            )
            adapter = upcomingAdapter
        }

        img_search.setOnClickListener {
            startActivity(
                Intent(this, SearchActivity::class.java)
            )
        }
    }

    /** Get now playing movies from server */
    private fun getNowPlaying() {
        mainViewModel.apply {
            fetchMovieList(MovieType.NOW_PLAYING).observe(this@MainActivity, {
                nowPlayingAdapter.submitList(it)
            })
            movieListLiveData.observe(this@MainActivity, {
                when {
                    it.isLoading -> loading.visibility = View.VISIBLE
                    it.isSuccess -> loading.visibility = View.GONE
                    it.isError -> {
                        loading.visibility = View.GONE
                        it.message?.let { it1 -> errorHandler(it1) }
                    }
                }
            })
        }
    }

    /** Get popular movies from server */
    private fun getPopular() {
        mainViewModel.apply {
            fetchMovieList(MovieType.POPULAR).observe(this@MainActivity, {
                popularAdapter.submitList(it)
            })
        }
    }

    /** Get top rated movies from server */
    private fun getTopRated() {
        mainViewModel.apply {
            fetchMovieList(MovieType.TOP_RATED).observe(this@MainActivity, {
                topRatedAdapter.submitList(it)
            })
        }
    }

    /** Get upcoming movies from server */
    private fun getUpcoming() {
        mainViewModel.apply {
            fetchMovieList(MovieType.UPCOMING).observe(this@MainActivity, {
                upcomingAdapter.submitList(it)
            })
        }
    }

    /** Show movie details */
    override fun onItemClick(item: Movie) {
        startActivity(
            Intent(this, DetailsActivity::class.java)
                .putExtra("movieId", item.id)
        )
    }
}