package com.example.imovies.ui.details

import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import androidx.lifecycle.viewModelScope
import com.example.imovies.data.repository.MainRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class DetailsViewModel @Inject constructor(private val mainRepository: MainRepository) :
    ViewModel() {

    /** Get movie list */
    val movieDetailsLiveData = ::fetchMovieDetails
    private fun fetchMovieDetails(movieId: Int) = mainRepository.getMovieDetails(movieId)
        .asLiveData(viewModelScope.coroutineContext)

    /** Get movie trailers */
    val trailersLiveData = ::fetchTrailers
    private fun fetchTrailers(movieId: Int) = mainRepository.getMovieTrailers(movieId)
        .asLiveData(viewModelScope.coroutineContext)

    /** Get movie cast */
    val castLiveData = ::fetchCast
    private fun fetchCast(movieId: Int) = mainRepository.getMovieCast(movieId)
        .asLiveData(viewModelScope.coroutineContext)
}