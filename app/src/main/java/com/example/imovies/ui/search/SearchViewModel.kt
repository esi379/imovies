package com.example.imovies.ui.search

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.example.imovies.app.Constants
import com.example.imovies.data.model.Keyword
import com.example.imovies.data.model.Movie
import com.example.imovies.data.model.PagingList
import com.example.imovies.data.model.Resource
import com.example.imovies.data.repository.MainRepository
import com.example.imovies.ui.search.datasource.ListFactory
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class SearchViewModel @Inject constructor(private val repository: MainRepository) : ViewModel() {

    /** Get keywords results */
    val keywordsLiveData = MutableLiveData<Resource<PagingList<Keyword>>>()
    fun fetchKeywords(query: String): LiveData<PagedList<Keyword>> {
        val factory = ListFactory(
            "keywords",
            query,
            repository,
            viewModelScope,
            keywordsLiveData
        )
        return LivePagedListBuilder(factory, Constants.PER_PAGE).build()
    }

    /** Get search results */
    val searchLiveData = MutableLiveData<Resource<PagingList<Movie>>>()
    fun fetchSearch(query: String): LiveData<PagedList<Movie>> {
        val factory = ListFactory(
            "search",
            query,
            repository,
            viewModelScope,
            searchLiveData
        )
        return LivePagedListBuilder(factory, Constants.PER_PAGE).build()
    }
}