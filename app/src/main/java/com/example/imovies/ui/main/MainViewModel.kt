package com.example.imovies.ui.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.example.imovies.app.Constants
import com.example.imovies.data.model.Movie
import com.example.imovies.data.model.PagingList
import com.example.imovies.data.model.Resource
import com.example.imovies.data.repository.MainRepository
import com.example.imovies.ui.main.datasource.MovieListFactory
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(private val mainRepository: MainRepository) : ViewModel() {

    /** Get movie list */
    val movieListLiveData = MutableLiveData<Resource<PagingList<Movie>>>()
    fun fetchMovieList(type:MovieType): LiveData<PagedList<Movie>> {
        val factory = MovieListFactory(
            type,
            mainRepository,
            viewModelScope,
            movieListLiveData
        )
        return LivePagedListBuilder(factory, Constants.PER_PAGE).build()
    }
}