package com.example.imovies.ui.main.datasource

import android.os.Build
import androidx.annotation.RequiresApi
import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import com.example.imovies.data.model.Movie
import com.example.imovies.data.model.PagingList
import com.example.imovies.data.model.Resource
import com.example.imovies.data.repository.MainRepository
import com.example.imovies.ui.main.MovieType
import kotlinx.coroutines.CoroutineScope

/**
 * Created by vv0z on 06.01.2022
 */
class MovieListFactory(
    private val type: MovieType,
    private val repository: MainRepository,
    private val scope: CoroutineScope,
    private val networkHandler: MutableLiveData<Resource<PagingList<Movie>>>,
) : DataSource.Factory<Int, Movie>() {

    private val movieListListLiveData = MutableLiveData<MovieListDataSource>()

    @RequiresApi(Build.VERSION_CODES.Q)
    override fun create(): DataSource<Int, Movie> {
        val dataSource = MovieListDataSource(type,repository, scope, networkHandler)
        movieListListLiveData.postValue(dataSource)
        return dataSource
    }
}