package com.example.imovies.ui.main

enum class MovieType {
    NOW_PLAYING,
    POPULAR,
    TOP_RATED,
    UPCOMING
}