package com.example.imovies.ui.search

import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.widget.ArrayAdapter
import androidx.activity.viewModels
import androidx.appcompat.widget.SearchView
import androidx.paging.PagedList
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.imovies.R
import com.example.imovies.data.model.Keyword
import com.example.imovies.data.model.Movie
import com.example.imovies.databinding.ActivitySearchBinding
import com.example.imovies.ui.base.BaseActivity
import com.example.imovies.ui.details.DetailsActivity
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_search.*

@AndroidEntryPoint
class SearchActivity : BaseActivity<ActivitySearchBinding>(), SearchAdapter.OnItemClickListener {

    private val searchViewModel: SearchViewModel by viewModels()
    private lateinit var autoCompleteSearch: SearchView.SearchAutoComplete
    private var searchItems: ArrayList<String> = arrayListOf()
    private lateinit var adapter: ArrayAdapter<String>

    private val searchAdapter: SearchAdapter by lazy {
        SearchAdapter()
    }

    override fun getLayoutId(): Int {
        return R.layout.activity_search
    }


    @SuppressLint("RestrictedApi")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        initUI()
    }

    @SuppressLint("RestrictedApi")
    private fun initUI() {
        autoCompleteSearch = search_view.findViewById(androidx.appcompat.R.id.search_src_text)
                as SearchView.SearchAutoComplete
        autoCompleteSearch.setDropDownBackgroundResource(R.color.white)
        autoCompleteSearch.setTextColor(Color.WHITE)
        autoCompleteSearch.threshold = 0
        adapter = ArrayAdapter<String>(
            this@SearchActivity,
            android.R.layout.simple_dropdown_item_1line, searchItems
        )
        autoCompleteSearch.setAdapter(adapter)

        autoCompleteSearch.setOnItemClickListener { _, _, position, _ ->
            autoCompleteSearch.setText(adapter.getItem(position) as String)
        }

        autoCompleteSearch.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(
                s: CharSequence?,
                start: Int,
                count: Int,
                after: Int,
            ) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (s.toString().length > 1)
                    getSearchResults(s.toString())
            }

            override fun afterTextChanged(editable: Editable?) {
                if (editable?.length!! > 1) {
                    getKeywords(editable.toString())
                } else {
                    searchItems = arrayListOf()
                    adapter = ArrayAdapter<String>(
                        this@SearchActivity,
                        android.R.layout.simple_dropdown_item_1line,
                        searchItems
                    )
                    adapter.clear()
                    autoCompleteSearch.setAdapter(adapter)
                }
            }
        })



        searchAdapter.setOnItemClickListener(this)
        rv_results.apply {
            layoutManager = LinearLayoutManager(this@SearchActivity)
            adapter = searchAdapter
        }
    }


    private fun getKeywords(query: String) {
        searchViewModel.apply {
            fetchKeywords(query).observe(this@SearchActivity, {})
            keywordsLiveData.observe(this@SearchActivity, {
                when {
                    it.isSuccess -> {
                        adapter.clear()
                        searchItems.clear()
                        it.data?.list?.forEach { item -> searchItems.add(item.name) }
                        adapter.addAll(searchItems)
                        adapter.notifyDataSetChanged()
                    }
                    it.isError -> {
                        it.message?.let { it1 -> errorHandler(it1) }
                    }
                }
            })
        }
    }

    private fun getSearchResults(searchQuery: String) {
        searchViewModel.apply {
            fetchSearch(searchQuery).observe(this@SearchActivity, {
                searchAdapter.submitList(it as PagedList<Movie>)
            })
            searchLiveData.observe(this@SearchActivity, {
                if (it.isError) {
                    it.message?.let { it1 -> errorHandler(it1) }
                }
            })
        }
    }

    override fun onItemClick(item: Movie) {
        startActivity(
            Intent(this, DetailsActivity::class.java)
                .putExtra("movieId", item.id)
        )
    }
}