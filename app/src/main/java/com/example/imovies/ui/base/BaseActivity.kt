package com.example.imovies.ui.base

import android.os.Bundle
import android.widget.Toast
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import com.example.imovies.R
import com.example.imovies.utils.isNetworkConnected

abstract class BaseActivity<T : ViewDataBinding> : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // Init data binding
        initDataBinding()
    }

    /** Initialize DataBinding */
    private fun initDataBinding() {
        viewDataBinding = DataBindingUtil.setContentView<T>(this, getLayoutId()).apply {
            lifecycleOwner = this@BaseActivity
            executePendingBindings()
        }
    }

    /** Abstract method for get the layout resource id */
    @LayoutRes
    abstract fun getLayoutId(): Int

    /** Instance of data binding */
    var viewDataBinding: T? = null
        private set

    /** Back to prev page */
    fun goBack(view: android.view.View) {
        onBackPressed()
    }

    /** Show network errors */
    fun errorHandler(message: String) {
        val toast = if (!isNetworkConnected(this)) {
            getString(R.string.label_no_internet)
        } else {
            message
        }
        Toast.makeText(this, toast, Toast.LENGTH_LONG).show()
    }
}