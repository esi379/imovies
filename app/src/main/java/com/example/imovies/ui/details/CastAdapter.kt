package com.example.imovies.ui.details

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.imovies.data.model.Cast
import com.example.imovies.databinding.CastListItemBinding
import java.util.*

class CastAdapter: RecyclerView.Adapter<CastAdapter.ItemViewHolder>() {

    private var itemList: MutableList<Cast> = mutableListOf()

    inner class ItemViewHolder(val binding: CastListItemBinding) :
        RecyclerView.ViewHolder(binding.root)

    fun setItems(newList: MutableList<Cast>) {
        val diffCallback = ItemDiffCallback(this.itemList, newList)
        val diffResult = DiffUtil.calculateDiff(diffCallback)
        this.itemList = LinkedList(newList)
        diffResult.dispatchUpdatesTo(this)
    }

    private fun getItem(position: Int): Cast {
        return itemList[position]
    }

    inner class ItemDiffCallback(
        private val mOldList: List<Cast>,
        private val mNewList: List<Cast>,
    ) : DiffUtil.Callback() {

        override fun getOldListSize(): Int {
            return mOldList.size
        }

        override fun getNewListSize(): Int {
            return mNewList.size
        }

        override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            return mOldList[oldItemPosition].name == mNewList[newItemPosition].name
        }

        override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            val oldItem = mOldList[oldItemPosition]
            val newItem = mNewList[newItemPosition]
            return (oldItem == newItem)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = CastListItemBinding.inflate(layoutInflater, parent, false)
        return ItemViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        val item = getItem(position)
        holder.binding.cast = item
    }

    override fun getItemCount(): Int {
        return itemList.size
    }
}