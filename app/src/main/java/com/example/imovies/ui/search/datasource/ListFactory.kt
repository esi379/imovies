package com.example.imovies.ui.search.datasource

import android.os.Build
import androidx.annotation.RequiresApi
import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import com.example.imovies.data.model.Movie
import com.example.imovies.data.model.PagingList
import com.example.imovies.data.model.Resource
import com.example.imovies.data.repository.MainRepository
import com.example.imovies.ui.main.MovieType
import kotlinx.coroutines.CoroutineScope

/**
 * Created by vv0z on 06.01.2022
 */
class ListFactory<T>(
    private val type: String,
    private val qury: String,
    private val repository: MainRepository,
    private val scope: CoroutineScope,
    private val networkHandler: MutableLiveData<Resource<PagingList<T>>>,
) : DataSource.Factory<Int, T>() {

    private val movieListListLiveData = MutableLiveData<ListDataSource<T>>()

    @RequiresApi(Build.VERSION_CODES.Q)
    override fun create(): DataSource<Int, T> {
        val dataSource = ListDataSource(type, qury, repository, scope, networkHandler)
        movieListListLiveData.postValue(dataSource)
        return dataSource
    }
}