package com.example.imovies.app

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

/**
 * Created by vv0z on 06.01.2022
 *
 * @Description : Main application class
 */
@HiltAndroidApp
class MyApplication:Application() {

}