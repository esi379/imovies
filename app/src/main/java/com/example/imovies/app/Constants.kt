package com.example.imovies.app

object Constants {

    const val AUTHENTICATION = "Authorization"
    const val TOKEN_TYPE = "Bearer"
    const val API_KEY =
        "eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiI2NTZjZTkzNzIyNzhlMTRhMjUxNzk2NDA0ZTI2YTU1NCIsInN1YiI" +
                "6IjYxZDZjOGI3ODk5ZGEyMDAxYjJhYTU3ZCIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uI" +
                "joxfQ.vhlCY1Hn24GKrpRXKxrX7OBm3hAWL5i20Rjkp-a9kks"


    const val PER_PAGE = 20
}
